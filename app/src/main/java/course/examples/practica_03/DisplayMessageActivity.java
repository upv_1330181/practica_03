package course.examples.practica_03;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class DisplayMessageActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent  = getIntent();
        String message = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);

        //Creamos vista para el mensaje
        TextView textView = new TextView(this);
        textView.setTextSize(40);
        textView.setText(message);
        //Configuramos la vista del texto como el layout de la actividad
        setContentView(textView);
    }
}
